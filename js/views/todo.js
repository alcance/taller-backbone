'user strict';

define(['backbone'], function(Backbone){
	console.log('Vista INICIALIZADA');
	var TodoView = Backbone.View.extend({

		el: '#item-template',

		// tagName: 'ul', 
		// className: 'container',
		// id: 'todos'

		todoTpl: _.template('Template de ejemplo'),

		events: {
			'dblick label': 'edit', 
			'click .toggle': 'toogleCompleted',
			'keypress .edit': 'updateOnEnter',
			'click .destroy': 'clear',
			'blur .edit': 'close'
		},		

		render: function(){
			this.$el.html(this.template(this.model.attributes));
			return this;
		},

		close: function(){
			// ejecuta cuando la tarea pierde foco
		},
		updateOnEnter: function(){
			// se ejectuta al presionar una tecla en la tarea 
		}
	});


	var todoView = new TodoView;

});




