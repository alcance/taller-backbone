//paths de configuracion para RequireJS
require.config({
	paths: {
		"jquery": "libs/jquery/dist/jquery",
		"underscore": "libs/underscore/underscore",
		"backbone": "libs/backbone/backbone"
	},
	shim: {
		"backbone": {
			//carga dependencias
			deps: ["jquery", "underscore"],
			//nombre personalizado para exportar
			exports: "Backbone"
		}
	},
	// cuanto esperar antes de sacar un error
	waitSeconds: 10
});

require(['jquery', 'underscore', 'backbone', 'models/todo', 'views/todo', 'routers/routers'], function(jquery, _, Backbone, App){
	return App;
});