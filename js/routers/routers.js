'use strict';

define(['backbone'], function(Backbone){
	var TodoRouter = Backbone.Router.extend({
		// definir rutas y mapear funciones
		routes: {
			"about": "showAbout",
			"search/:query" : "searchTodos",
			"search/:query/p:page" : "searchTodos"
		},

		showAbout: function(){
			console.log("Sobre Backbone");
		},

		searchTodos: function(query, page){
			var page_number = page || 1;
			console.log("Pagina numero: " + page_number + " de los resultados con la palabra: " + query);
		}
	});

	var myTodoRouter = new TodoRouter();

	Backbone.history.start();
});