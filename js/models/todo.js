'use strict';

define(['backbone'], function(Backbone){
	var Todo = Backbone.Model.extend({
		initialize: function() {
			console.log("Modelo Todo inicializado");
			this.on('change', function(){
				console.log('El Valor de este modelo ha sido modificado');
			});
		},
		defaults: {
			'title': '',
			'completed': false
		},
		validate: function(attributes){
			if(atributes.title === undefined) {
				return "Recuerda especificar un titulo a la tarea";
			}
		}
	});

	var myTodo = new Todo();

	myTodo.set({
		title: 'Comprar Libro Backbone',
		completed: true
	});	

	console.log('Se ha cambiado el titulo ' + myTodo.get('title'));	
});